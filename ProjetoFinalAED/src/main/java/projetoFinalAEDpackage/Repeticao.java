package projetoFinalAEDpackage;

public class Repeticao {
	String id;
	int hash;
	Repeticao proximoRepeticao;
	Nome nome;

	public Repeticao(String id, Nome nome) {

		this.id  						= id;
		this.nome		 				= nome;
		this.hash 						= id.hashCode();
	}

	public static void imprimir(Repeticao raizRepeticao) {
		raizRepeticao = raizRepeticao.proximoRepeticao;
		while (raizRepeticao != null) {
			System.out.print(raizRepeticao.id + " ");
			while (raizRepeticao.nome != null) {
				System.out.print(raizRepeticao.nome.nome + "; ");

				raizRepeticao.nome = raizRepeticao.nome.proximoNome;
			}
			System.out.println("");

			raizRepeticao = raizRepeticao.proximoRepeticao;
		}


	}
	public static void adicionar(Repeticao raizRepeticao, Pessoa pessoaNova, Pessoa outraPessoaNova) {

		Repeticao idRepetido = raizRepeticao.proximoRepeticao;

		while (idRepetido != null) {
			if (idRepetido.hash == pessoaNova.hash) {
					Nome.adicionar(idRepetido, pessoaNova.nome);
					return;
			} else {
				idRepetido = idRepetido.proximoRepeticao;
			}
		}

		idRepetido = new Repeticao(pessoaNova.id, new Nome(pessoaNova.nome));
		idRepetido.nome.proximoNome = new Nome(outraPessoaNova.nome);

		idRepetido.proximoRepeticao		= raizRepeticao.proximoRepeticao;
		raizRepeticao.proximoRepeticao	= idRepetido;
	}


}
