package projetoFinalAEDpackage;

public class Nome {
    String nome;
    Nome proximoNome;

    public Nome(String nome) {

        this.nome		 = nome;
        this.proximoNome		 = null;
    }

    public static void adicionar(Repeticao idRepetido, String nome) {

        Nome nomeAinserir = new Nome(nome);

        nomeAinserir.proximoNome = idRepetido.nome.proximoNome;

        idRepetido.nome.proximoNome=nomeAinserir;

    }

}
