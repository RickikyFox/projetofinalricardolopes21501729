package projetoFinalAEDpackage;

public class Pessoa {

    Pessoa ramoEsquerdo;
    Pessoa ramoDireito;


    String id, nome;
    int hash;

    public Pessoa(String nome, String id) {

        this.nome   = nome;
        this.id		 = id;
        this.hash = id.hashCode();
    }

    public static void adicionaPessoa(Pessoa raizPessoas, String nome, String id, Repeticao raizRepeticao) {
        //preenche a arvore de pessoas. Se ao preencher encontrar alguem igual, come�a a preencher a lista de repeticoes

        Pessoa raiz = raizPessoas;

        Pessoa pessoaNova = new Pessoa(nome, id);

        while(true) {
            if (pessoaNova.hash > raiz.hash)
                if (raiz.ramoDireito == null) {
                    raiz.ramoDireito = pessoaNova;
                    return;
                } else {
                    raiz = raiz.ramoDireito;
                }
            if (pessoaNova.hash < raiz.hash)
                if (raiz.ramoEsquerdo == null) {
                    raiz.ramoEsquerdo = pessoaNova;
                    return;
                } else {
                    raiz = raiz.ramoEsquerdo;
                }
            if (pessoaNova.hash == raiz.hash) {
                Repeticao.adicionar(raizRepeticao, pessoaNova, raiz);
                return;
            }
        }
    }



}
