package projetoFinalAEDpackage;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Main{

    public static void main(String[] args) {
    	
    	long tempo=System.currentTimeMillis();

        int indexDepartamentos,
                indexCargos,
                salarioPessoaX = 0,                                 //pergunta1
                totalSalariosDepartamentoDez = 0,                   //pergunta2
                totalSalariosCargoTres = 0,                         //pergunta3
                salarioMaisAlto = 0,                                //pergunta4
                salarioMaisBaixo = 32766,                           //pergunta6 MAX int
                totalPessoasComSalarioMaisAlto = 0,                 //pergunta5
                cargoComSalarioMaisAlto = 0,
                totalPessoasComSalarioMaisBaixo = 0,                //pergunta7
                cargoComSalarioMaisBaixo = 0,
                departamentoMaisCaro = 0,                           //pergunta8
                departamentoMaisCaroCodigo = 0,
                departamentoMaisBarato = 32767,                     //pergunta9 MAX int
                departamentoMaisBaratoCodigo = 0,

                salarioAcomparar,
                cargoAcomparar,
                departamentoAComparar;
        String nomeAcomparar,
                idAcomparar;


        Departamento[] departamentos = new Departamento[11];    //pergunta8e9
        for (indexDepartamentos = 1; indexDepartamentos < 11; indexDepartamentos++)
            departamentos[indexDepartamentos] = new Departamento();

        Cargo[] cargos = new Cargo[16];
        for (indexCargos = 1; indexCargos < 16; indexCargos++)
            cargos[indexCargos] = new Cargo();


        Repeticao raizRepeticao = new Repeticao("0", null);                            //pergunta10
        Pessoa raizPessoa = new Pessoa(null, "0");
        raizPessoa.hash = 0;



        Scanner in;
        try {
            in = new Scanner(new File("C:\\Users\\Ricky\\Desktop\\salarios.txt")).useDelimiter("\\s*:\\s*|\\s*\n");
        } catch (FileNotFoundException NoFile) {
            System.out.println(NoFile.getMessage());
            return;
        }

        indexCargos=0;
        while (in.hasNextLine()) {
            indexCargos++;

            in.nextInt();

            salarioAcomparar = in.nextInt();

            cargos[indexCargos].salario = salarioAcomparar;
        }


        try {
            in = new Scanner(new File("C:\\Users\\Ricky\\Desktop\\funcionarios.txt")).useDelimiter("\\s*:\\s*|\\s*\n");
        } catch (FileNotFoundException NoFile) {
            System.out.println(NoFile.getMessage());
            return;
        }


        while (in.hasNextLine()) {
            nomeAcomparar = in.next();
            idAcomparar = in.next();
            departamentoAComparar = in.nextInt();
            cargoAcomparar = in.nextInt();
            salarioAcomparar = cargos[cargoAcomparar].salario;



            if (idAcomparar.equals("12340050"))
                salarioPessoaX = salarioAcomparar;

            //////////////////////////////
            //pergunta 1 respondida//
            /////////////////////////////

            if (departamentoAComparar == 10)
                totalSalariosDepartamentoDez += salarioAcomparar;

            //////////////////////////////
            //pergunta 2 respondida//
            /////////////////////////////

            if (cargoAcomparar == 3)
                totalSalariosCargoTres += salarioAcomparar;

            //////////////////////////////
            //pergunta 3 respondida//
            /////////////////////////////


            if (salarioAcomparar > salarioMaisAlto) {
                salarioMaisAlto = salarioAcomparar;
                totalPessoasComSalarioMaisAlto=0;
                cargoComSalarioMaisAlto = cargoAcomparar;
            }

            //////////////////////////////
            //pergunta 4 respondida//
            /////////////////////////////

            if (salarioAcomparar < salarioMaisBaixo) {
                salarioMaisBaixo = salarioAcomparar;
                totalPessoasComSalarioMaisBaixo=0;
                cargoComSalarioMaisBaixo = cargoAcomparar;
            }

            //////////////////////////////
            //pergunta 6 respondida//
            /////////////////////////////


            if (cargoAcomparar == cargoComSalarioMaisAlto)
                totalPessoasComSalarioMaisAlto++;

            //////////////////////////////
            //pergunta 5 respondida//
            /////////////////////////////

            if (cargoAcomparar == cargoComSalarioMaisBaixo)
                totalPessoasComSalarioMaisBaixo++;

            //////////////////////////////
            //pergunta 7 respondida//
            /////////////////////////////

            departamentos[departamentoAComparar].totalSalarios += salarioAcomparar;

            Pessoa.adicionaPessoa(raizPessoa, nomeAcomparar, idAcomparar, raizRepeticao);

            //////////////////////////////
            //pergunta 10 respondida//
            /////////////////////////////


        }//ACABOU A LEITURA DE FUNCIONARIOS


        for (indexDepartamentos = 1; indexDepartamentos < departamentos.length; indexDepartamentos++) {

            if (departamentos[indexDepartamentos].totalSalarios>departamentoMaisCaro) {
                departamentoMaisCaro=departamentos[indexDepartamentos].totalSalarios;
                departamentoMaisCaroCodigo = indexDepartamentos;

            }
            if(departamentos[indexDepartamentos].totalSalarios !=0){
            	if(departamentos[indexDepartamentos].totalSalarios<departamentoMaisBarato){
                departamentoMaisBarato=departamentos[indexDepartamentos].totalSalarios;
                departamentoMaisBaratoCodigo = indexDepartamentos;
            }
            }

            //////////////////////////////
            //pergunta 8 e 9 respondida//
            /////////////////////////////
        }


        System.out.println(salarioPessoaX);
        System.out.println(totalSalariosDepartamentoDez);
        System.out.println(totalSalariosCargoTres);
        System.out.println(salarioMaisAlto);
        System.out.println(totalPessoasComSalarioMaisAlto);
        System.out.println(salarioMaisBaixo);
        System.out.println(totalPessoasComSalarioMaisBaixo);
        System.out.println(departamentoMaisCaroCodigo);
        System.out.println(departamentoMaisBaratoCodigo);
       
        Repeticao.imprimir(raizRepeticao);

        
        System.out.println("Tempo de execucao: "+(System.currentTimeMillis()-tempo)+"ms");
        
    }

    }

